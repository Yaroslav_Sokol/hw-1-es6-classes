// 1. Прототипне наслідування - це коли один об'єкт має якусь властивість, і він є прототипом іншого об'єкту, то цей інший об'єкт успадкує
//    властивості першого об'єкту, і може використовувати їх як свої власні. 
//    Якщо легше то об'єкт дозволяє використовувати властивості і методи іншого об'єкта як свої власні.
// 
// 2. Для виклику конструктора з його батьківського класу, типу якщо в батьківському класі є "this.name = name;" то в дочірньому до цього класу
//    цей name можна викликати як "super(name);"

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }

    set name(name) {
        this._name = name;
    }

    set age(age) {
        this._age = age;
    }
    
    set salary(salary) {
        this._salary = salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age);
        this._salary = salary * 3;
        this._lang = lang;
    }

    get lang() {
        return this.lang;
    }

    set lang(lang) {
        this._lang = lang;
    }

    set salary(salary) {
        this._salary = salary * 3;
    }

    // В завданні не було, але нехай буде
    get programmerInfo() {
        console.log(`Name: ${this._name}, age: ${this._age}, salary: ${this._salary}, programing language: ${this._lang}`);
    }
}

// Перший користувач
const programmer1 = new Programmer("Yaroslav", 17, 18000, "Java Script");
console.log(programmer1);
console.log(programmer1.programmerInfo); // Вивів і такий варіант, і такий

// Другий користувач 
const programmer2 = new Programmer("Maks", 22, 40000, "Python");
console.log(programmer2);
console.log(programmer2.programmerInfo); // Вивів і такий варіант, і такий

// Третій користувач 
const programmer3 = new Programmer("Sasha", 19, 28000, "C++");
console.log(programmer3);
console.log(programmer3.programmerInfo); // Вивів і такий варіант, і такий

// Перевірка сетерів :)
programmer3.name = "Denis";
programmer3.age = "20";
programmer3.salary = "24000";
programmer3.lang = "Java";
console.log(programmer3);
console.log(programmer3.programmerInfo); // Вивів і такий варіант, і такий